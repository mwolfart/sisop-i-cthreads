#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"


void* compra1 (void* ticket);
void* compra2 (void* ticket);

csem_t mutex;

int plane_ticket = 1;

int main() {

	int t1,t2;

	csem_init(&mutex, 1);
	
	int ticket1;
	int ticket2;

	t1 = ccreate(compra1,(void*)&ticket1,0);
	t2 = ccreate(compra2,(void*)&ticket2,0);

	printf("Eu sou a main indo esperar por t1 e t2\n");

	cjoin(t1);
	cjoin(t2);
	
	printf("O passageiro 1 voa no banco %d\n", ticket1);
	printf("O passageiro 2 voa no banco %d\n", ticket2);
	
	printf("O proximo ticket vale %d\n", plane_ticket);

	printf("Eu sou a main terminando...\n");
	
	
	return 0;
}

void* compra1 (void* ticket)
{
	printf("Eu sou a t1 executando!\n");
	printf("Eu sou a t1 tentando entrar em minha secao critica ...\n");
	cwait(&mutex);
	printf("Eu sou a t1 executando a minha secao critica...\n");
	*(int*)ticket = plane_ticket;
	printf("Eu sou a t1 abrindo mao da execucao...\n");
	cyield();
	printf("Eu sou a t1 passando o ticket para o proximo..\n");
	plane_ticket++;
	printf("Eu sou a t1 terminando minha secao critica...\n");
	csignal(&mutex);

	printf("t1 termina\n");
	
	return NULL;
}


void* compra2 (void* ticket)
{
	printf("Eu sou a t2 executando!\n");
	printf("Eu sou a t2 tentando entrar em minha secao critica ...\n");
	cwait(&mutex);
	printf("Eu sou a t2 executando a minha secao critica...\n");
	*(int*)ticket = plane_ticket;
	printf("Eu sou a t2 abrindo mao da execucao...\n");
	cyield();
	printf("Eu sou a t2 passando o ticket para o proximo..\n");
	plane_ticket++;
	printf("Eu sou a t2 terminando minha secao critica...\n");
	csignal(&mutex);

	printf("t2 termina\n");

	return NULL;

}

