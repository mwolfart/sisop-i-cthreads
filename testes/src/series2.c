
/*
 * Exemplo de teste.
 * O objetivo desse teste é testar ccreate, cjoin, csem_init, cwait, cyield e csignal.
 */
#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

#define PA_AMT 8
#define PG_AMT 10
#define FIB_AMT 12
#define TRI_AMT 6

void* prog_a (void* teste);
void* prog_g (void* teste);

csem_t mutex;

int a,b;

int main()
{	
	a = ccreate (prog_a, NULL, 0);
	b = ccreate (prog_g, NULL, 0); //This was tested with if/elses earlier

	csem_init(&mutex,1);
	
	printf("Thread main espera pela thread a terminar !\n");	
	cjoin(a);
	printf("Thread main espera pela thread b terminar !\n");
	cjoin(b);
	
	printf("Main ends!\n");

	return 0;

}

void* prog_a (void* test) {
	
	int ai=1, i=0;

	for(;i<PA_AMT;i++)					// Iteração "manual"
	{
		printf("PA termo %d : %d\n",i+1, ai);
		ai += 4;
		if (i==3)
		{
			printf("Thread 1 tenta entrar em secao critica!\n");
			cwait(&mutex);
			printf("Thread 1 em secao critica!\n");
			printf("Thread 1 faz yield !\n");
			cyield();
			printf("Thread 1 sai da secao critica!\n");
			csignal(&mutex);
			
		}
	}
	
	
	return NULL;
}

void* prog_g (void* teste)
{
	int ai=1, i=0;

	for (;i<PG_AMT;i++)		// Iteração "manual"
	{
		printf("PG termo %d : %d\n",i+1, ai);
		ai *= 2;
		if (i==3)
		{
			printf("Thread 2 tenta entrar em secao critica!\n");
			cwait(&mutex);
			printf("Thread 2 em secao critica!\n");
			printf("Thread 2 faz yield !\n");
			cyield();
			printf("Thread 2 sai da secao critica!\n");
			csignal(&mutex);
			
		}
	}


	return NULL;
}


