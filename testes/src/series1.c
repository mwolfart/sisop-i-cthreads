
/*
 * Exemplo de teste.
 * O objetivo desse teste é simular o trabalho parcial 2 mas agora utilizando a biblioteca cthreads.
 */
#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

#define PA_AMT 8
#define PG_AMT 10
#define FIB_AMT 12
#define TRI_AMT 6

void* fibonacci (void* teste);
void* prog_a (void* teste);
void* prog_g (void* teste);
void* triangulares(void* teste);

int rotate = 0;
int a,b,c,d;
int main()
{	
	a = ccreate (prog_a, NULL, 0);
	b = ccreate (prog_g, NULL, 0); //This was tested with if/elses earlier
	c = ccreate (fibonacci, NULL, 0);
	d = ccreate (triangulares, NULL, 0);

	cjoin(c);
	
	printf("Main ends!\n");

	return 0;

}

void* prog_a (void* test) {
	
	int ai=1, i=0;

	for(;i<PA_AMT;i++)					// Iteração "manual"
	{
		printf("PA termo %d : %d\n",i+1, ai);
		ai += 4;
		cyield();

	}
	
	
	return NULL;
}

void* prog_g (void* teste)
{
	int ai=1, i=0;

	for (;i<PG_AMT;i++)		// Iteração "manual"
	{
		printf("PG termo %d : %d\n",i+1, ai);
		ai *= 2;
		cyield();
	}


	rotate = 1;
	return NULL;
}

void* fibonacci (void* teste)
{

	int f0=1,f1=1, i=0, faux;

	
	for(;i < FIB_AMT;i++) {			// Iteração "manual"

		if (i==0)
			printf("Fibo termo 1: 1\n");
		else if (i==1)
			printf("Fibo termo 2: 1\n");
		else {
			faux = f1;
			f1 += f0;
			f0 = faux;
			printf("Fibo termo %d: %d\n",i+1, f1);
		}
	
		cyield();
	}

	return NULL;
}

void* triangulares(void* teste)
{
	int sum=0, i=1;
	
	for (;i <= TRI_AMT;i++) {			// Iteração "manual"
		sum += i;
		printf("Tri termo %d : %d\n",i, sum);
		cyield();
	}
	
	
	return NULL;
}
